ALTER TABLE plugin_analytique_recherches RENAME TO plugin_analytique_recherches_old;

CREATE TABLE IF NOT EXISTS plugin_analytique_recherches (
	id					INTEGER PRIMARY KEY,
	date_start		TEXT NOT NULL, 
	date_end		TEXT NOT NULL, 

	FOREIGN KEY(id) REFERENCES searches(id)
);

INSERT OR IGNORE INTO plugin_analytique_recherches (id, date_start, date_end)
	SELECT o.id, o.date_start, o.date_end
	FROM plugin_analytique_recherches_old o;

DROP TABLE plugin_analytique_recherches_old;
