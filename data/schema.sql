CREATE TABLE IF NOT EXISTS plugin_analytique_recherches (
	id					INTEGER PRIMARY KEY,
	date_start		TEXT NOT NULL, 
	date_end		TEXT NOT NULL, 

	FOREIGN KEY(id) REFERENCES searches(id)
);
