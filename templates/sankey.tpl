{include file="_head.tpl" title="Diagramme de Sankey — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="sankey"}


<div class="year-header">
    <p>Cette page génère du code pour créer un diagramme de Sankey interractif, et génère le diagramme en intégrant <a href="http://sankeymatic.com/build/">SankeyMATIC</a>.</p>
    <p>Les données comptables prises en comptes sont l'ensemble des projets sur le plan comptable utilisé.</p>
    <br>
    <p>La disposition des noeuds est automatique et consistante pour des mêmes paramètres/données. Il peut être nécessaire de les réorganiser pour une meilleure lisibilité, mais changer certains paramètres donnent aussi une autre disposition (par exemple, node width, ou l'ordre des lignes dans les données).</p>
    <p>Les paramètres de SankeyMATIC ne sont pas enregistrés.</p>
</div>

<form method="post" action={$self_url}>
<fieldset>
    <legend>Paramètres</legend>

    {input type="checkbox" name="display_codes" value=1 label="Afficher le code des comptes" source=$params}
    <p><small>(chez moi, décocher cette case créé un layout totalement éclaté, du coup je laisse les codes puis édite le SVG avec <code>:%s/\v(\d*|[A-Z0-9]*) - //g </code>)</small></p>
    <br>
    <p>Entrez l'id (et pas le code) des comptes de charges/produits réfléchis. Pour l'instant, qu'un seul compte de chaque est supporté.</p>
    {input type="number" name="charges" label="ID du compte de charges réfléchies" source=$reflechi}
    {input type="number" name="produits" label="ID du compte de produits réfléchis" source=$reflechi}
    <br>
    <p>Regrouper les sous-comptes de charges/produits ensemble (ex: comptes 6312, 6313, 63131, seront regroupés dans un noeud 631).</p>
    {input type="checkbox" name="group_charges" value=1 label="Regrouper les charges" source=$group}
    {input type="checkbox" name="group_produits" value=1 label="Regrouper les produits" source=$group}
    <br>
    {input type="textarea" name="extra_lines" label="Ajouter des lignes supplémentaires (pour coloration, ... voir manuel)" source=$params}

    <p class="submit">
        {csrf_field key="select_accounts"}
        <input type="submit" name="select" class="main" value="Enregistrer &rarr;" />
    </p>
</fieldset>
</form>

<hr>

<div id="sankeymatic_wrapper">

<script src="sk/d3.v2.js"></script> 
<script src="sk/sankey.js"></script> 
<script src="sk/canvg.js"></script> 
<script src="sk/rgbcolor.js"></script> 
<script src="sk/sankeymatic.js"></script>
<header>
    <strong><a title="SankeyMATIC home page" href="http://sankeymatic.com/">SankeyMATIC (BETA)</a></strong>
    <a class="button" title="How to use SankeyMATIC to the fullest" href="http://sankeymatic.com/manual/">Manuel</a>
    <a class="button" title="Gallery of Sankey Examples" href="http://sankeymatic.com/gallery/">Gallerie</a>
    <a class="button" title="Frequently Asked Questions" href="http://sankeymatic.com/faq/">FAQ</a>
</header>

<div class="center_basic">
    <table>
        <tr>
            <td>
                    <table class="center_basic">
                        <tr>
                            <td id="other_entry">
                                <h2 class="ui_head" onclick="toggle_panel('input_options');"><span id="input_options_indicator" class="indicator">⇓</span> Données d'entrée<span id="input_options_hint">:</span></h2>

                                <form onsubmit="process_sankey(); return false;">
                                    <div id="input_options">
                                        <textarea id="flows_in" rows="22" cols="40" onchange="process_sankey();">{$text}</textarea>
                                        <p class="center_basic"><button id="preview_graph" type="submit">Prévisualiser</button>
                                        </p>
                                    </div>
                                </form>
                                
                                <!-- SIZE & SPACING -->

                                <h2 class="ui_head" onclick="toggle_panel('diagram_options');"><span id="diagram_options_indicator" class="indicator">⇑</span> Taille, espacement et formes<span id="diagram_options_hint">:</span></h2>

                                <div id="diagram_options" class="form_chunk">
                                    <p class="form_elements1"><label for="canvas_width"><strong>Largeur du diagramme :</strong></label> <span class="no_wrap"><input type="text" class="wholenumber" id="canvas_width" size="5" maxlength="6"
                                    value="1100" onchange="process_sankey();"> px</span> <label for="canvas_height" class="spaced_label"><strong>Hauteur :</strong></label> <span class="no_wrap"><input type="text" class="wholenumber" id=
                                    "canvas_height" size="5" maxlength="6" value="1000" onchange="process_sankey();"> px</span></p>

                                    <p class="form_elements2"><label for="node_padding"><strong>Espace vertical entre les noeuds :</strong></label> <span class="no_wrap"><input type="text" class="wholenumber" id="node_padding" size="2"
                                    maxlength="4" value="18" onchange="process_sankey();"> px</span></p>

                                    <p class="form_elements1"><label for="node_width"><strong>Largeur des noeuds:</strong></label> <span class="no_wrap"><input type="text" class="wholenumber" id="node_width" size="2" maxlength="4" value="10"
                                    onchange="process_sankey();"> px</span> <label for="curvature" class="spaced_label"><strong>Courbure :</strong></label>&nbsp; <span class="no_wrap">|<input id="curvature" type="range" min="0.05" max=
                                    "0.8" step="0.15" value="0.5" onchange="process_sankey();" class="narrowslider">(</span><br></p>

                                    <p class="form_elements2"><strong>Marges :</strong> <span class="no_wrap"><label for="top_margin">Haut</label> <input type="text" class="wholenumber" id="top_margin" size="2" maxlength="4" value="12"
                                    onchange="process_sankey();"></span> <span class="no_wrap"><label for="bottom_margin">Bas</label> <input type="text" class="wholenumber" id="bottom_margin" size="2" maxlength="4" value="12" onchange=
                                    "process_sankey();"></span> <span class="no_wrap"><label for="left_margin">Gauche</label> <input type="text" class="wholenumber" id="left_margin" size="2" maxlength="4" value="12" onchange=
                                    "process_sankey();"></span> <span class="no_wrap"><label for="right_margin">Droite</label> <input type="text" class="wholenumber" id="right_margin" size="2" maxlength="4" value="12" onchange=
                                    "process_sankey();"></span></p>
                                </div>
                                <!-- COLORS -->

                                <h2 class="ui_head" onclick="toggle_panel('color_options');"><span id="color_options_indicator" class="indicator">⇓</span> Couleurs<span id="color_options_hint">...</span></h2>

                                <div id="color_options" class="form_chunk" style="display: none;">
                                    <p class="form_elements2"><label for="background_color"><strong>Couleur du fond :</strong></label> <input type="color" id="background_color" size="7" maxlength="7" value="#FFFFFF" onchange=
                                    "process_sankey();"> <input type="checkbox" id="background_transparent" value="1" onchange="process_sankey();" class="spaced_checkbox"><label for="background_transparent">Transparent</label><br></p>

                                    <p class="form_elements1"><strong>Couleur des noeuds:</strong> Utiliser un thème : <input name="default_node_colorset" type="radio" id="node_use_set_a" value="A" onchange="process_sankey();"><label for=
                                    "node_use_set_a">A</label> <input name="default_node_colorset" type="radio" id="node_use_set_b" value="B" onchange="process_sankey();"><label for="node_use_set_b">B</label> <input name=
                                    "default_node_colorset" type="radio" id="node_use_set_c" value="C" onchange="process_sankey();" checked="checked"><label for="node_use_set_c">C</label><br>
                                    <input name="default_node_colorset" type="radio" id="node_use_color" value="none" onchange="process_sankey();"><label for="node_use_color">Utiliser une couleur unique :</label> <input type="color" id=
                                    "default_node_color" size="7" maxlength="7" value="#004477" onchange="process_sankey();"><br></p>

                                    <p class="form_elements2"><strong>Couleur des liens :</strong><br>
                                    <input name="default_flow_inherit" type="radio" id="flow_inherit_from_source" value="source" onchange="process_sankey();"> <label for="flow_inherit_from_source">Utiliser la couleur du noeud source
                                    </label><br>
                                    <input name="default_flow_inherit" type="radio" id="flow_inherit_from_target" value="target" onchange="process_sankey();" checked="checked"> <label for="flow_inherit_from_target">Utiliser la couleur du noeud cible</label><br>
                                    <input name="default_flow_inherit" type="radio" id="flow_use_color" value="none" onchange="process_sankey();"> <label for="flow_use_color">Utiliser une couleur unique :</label> <input type="color" id=
                                    "default_flow_color" size="7" maxlength="7" value="#666666" onchange="process_sankey();"></p>

                                    <p class="form_elements1"><label for="default_flow_opacity"><strong>Opacité des liens :</strong></label> <span class="no_wrap"><span class="smalllabel">0.0</span><input id="default_flow_opacity" type=
                                    "range" min="0" max="1" step="0.1" value="0.3" onchange="process_sankey();" class="narrowslider"><span class="smalllabel">1.0</span></span></p>
                                </div>
                                <!-- LABELS -->

                                <h2 class="ui_head" onclick="toggle_panel('label_options');"><span id="label_options_indicator" class="indicator">⇓</span> Libéllés & unités<span id="label_options_hint">...</span></h2>

                                <div id="label_options" class="form_chunk" style="display: none;">
                                    <p class="form_elements1"><input type="checkbox" id="show_labels" value="1" onchange="process_sankey();" checked="checked"> <label for="show_labels">Afficher les libellés</label> <small>(désactivez pour afficher votre propre texte sur le diagramme)</small><br>
                                    <input type="checkbox" id="include_values_in_node_labels" value="1" onchange="process_sankey();" checked="checked"> <label for="include_values_in_node_labels">Afficher le total des noeuds dans le libéllé</label><br>
                                    <input type="checkbox" id="display_full_precision" value="1" onchange="process_sankey();" checked="checked" class="spaced_label"> <label for="display_full_precision">Toujours afficher les chiffres après la virgules<small>(utile pour de la monnaie)</small></label><br></p>

                                    <p class="form_elements2"><strong>Typographie :</strong> <span class="no_wrap"><input name="font_face" type="radio" id="sans_serif" value="sans-serif" checked="checked" onchange=
                                    "process_sankey();"><label for="sans_serif" style="font-family: sans-serif;">sans</label></span> <span class="no_wrap"><input name="font_face" type="radio" id="serif" value="serif" onchange=
                                    "process_sankey();"><label for="serif" style="font-family: serif;">serif</label></span> <span class="no_wrap"><input name="font_face" type="radio" id="monospace" value="monospace" onchange=
                                    "process_sankey();"><label for="monospace" style="font-family: monospace;">mono</label></span> <label for="font_size" class="spaced_label"><strong>Taille :</strong></label> <span class=
                                    "no_wrap"><input type="text" class="wholenumber" id="font_size" size="2" maxlength="4" value="12" onchange="process_sankey();"> <label for="font_size">px</label></span></p>

                                    <p class="form_elements1"><label for="font_color"><strong>Couleur :</strong></label> <input type="color" id="font_color" size="7" maxlength="7" value="#000000" onchange="process_sankey();"> <label for=
                                    "font_weight" class="spaced_label"><strong>Épaisseur :</strong></label> <span class="no_wrap"><span style="font-weight: lighter;" class="smalllabel">Fin</span><input id="font_weight" type="range" min=
                                    "100" max="700" step="300" value="100" onchange="process_sankey();" class="narrowslider"><span style="font-weight: bolder;" class="smalllabel">Gras</span></span><br></p>

                                    <p class="form_elements2"><strong>Unités :</strong> <span class="no_wrap"><label for="unit_prefix">Préfixe =</label> <input type="text" id="unit_prefix" size="3" maxlength="10" onchange=
                                    "process_sankey();"></span> <span class="no_wrap"><label for="unit_suffix">Suffixe =</label> <input type="text" id="unit_suffix" size="5" maxlength="10" value="€" onchange="process_sankey();"></span></p>

                                    <p class="form_elements1"><label for="number_format"><strong>Format numérique :</strong></label> <select id="number_format" onchange="process_sankey();">
                                        <option value=",.">
                                            1,000,000.00
                                        </option>
                                        <option value=".,">
                                            1.000.000,00
                                        </option>
                                        <option value=" .">
                                            1 000 000.00
                                        </option>
                                        <option value=" ," selected="selected">
                                            1 000 000,00
                                        </option>
                                        <option value="X.">
                                            1000000.00
                                        </option>
                                        <option value="X,">
                                            1000000,00
                                        </option>
                                    </select></p>
                                </div>
                                <!-- ADVANCED -->

                                <h2 class="ui_head" onclick="toggle_panel('advanced_options');"><span id="advanced_options_indicator" class="indicator">⇓</span> Advancé<span id="advanced_options_hint">...</span></h2>

                                <div id="advanced_options" class="form_chunk" style="display: none;">
                                    <p class="form_elements1"><input type="checkbox" id="flow_cross_check" value="1" onchange="process_sankey();" checked="checked"> <label for="flow_cross_check"><strong>Vérification des liens :</strong>
                                    Vérifier que les valeurs d'entrée/sortie des noeuds concordent.</label><br></p>

                                    <p class="form_elements2"><strong>Échelle du diagramme</strong> = <span id="scale_figures"></span><br>
                                    (Pour une comparaison honnête des diagrammes, faites correspondre leurs unités et échelles au mieux.)</p>

                                    <p class="form_elements1"><strong>Note :</strong> Vous pouvez <u>déplacer</u> les noeuds pour les repositionner<br>
                                    avant l'export. <br>(Cependant, changer un paramètre redessinera l'entiereté du diagramme.)</p>

                                    <p class="form_elements2"><label for="default_node_opacity"><strong>Opacité des noeuds :</strong></label> <span class="no_wrap"><span class="smalllabel">0.0</span><input id="default_node_opacity" type=
                                    "range" min="0" max="1" step="0.1" value="0.9" onchange="process_sankey();" class="narrowslider"><span class="smalllabel">1.0</span></span> <label for="node_border" class=
                                    "spaced_label"><strong>Contour :</strong></label> <select id="node_border" onchange="process_sankey();">
                                        <option value="0" selected="selected">
                                            0px
                                        </option>
                                        <option value="1">
                                            1px
                                        </option>
                                        <option value="2">
                                            2px
                                        </option>
                                        <option value="3">
                                            3px
                                        </option>
                                        <option value="4">
                                            4px
                                        </option>
                                        <option value="5">
                                            5px
                                        </option>
                                    </select></p>

                                    <p class="form_elements1"><input type="checkbox" id="reverse_graph" value="1" onchange="process_sankey();"> <label for="reverse_graph"><strong>Inverser le diagramme</strong> (droite/gauche)</label></p>
                                </div>

                                <h2 class="ui_head" onclick="toggle_panel('export_options');"><span id="export_options_indicator" class="indicator">⇑</span> Exporter le diagramme<span id="export_options_hint">:</span></h2>

                                <div id="export_options" class="form_chunk">
                                    <p class="form_elements" style="padding:4px;">Une fois que vous êtes satisfait de votre diagramme, vous pouvez <strong>l'exporter</strong> en image ou en vectoriel (SVG) :</p>

                                    <h3 class="ui_head" onclick="toggle_panel('png_options');"><span id="png_options_indicator" class="indicator">⇓</span>Image <abbr title="Portable Network Graphics">PNG</abbr><span id=
                                    "png_options_hint">...</span></h3>

                                    <div id="png_options" class="form_chunk" style="display: none;">
                                        <div class="form_elements1">
                                            <p class="center_para"><strong>Échelle :</strong> <span class="no_wrap"><input name="scale_x" type="radio" value="1" id="scale_1x" onchange="render_updated_outputs();"><label for=
                                            "scale_1x">1x&nbsp;(Basique)</label></span> <span class="no_wrap"><input name="scale_x" type="radio" value="2" id="scale_2x" onchange="render_updated_outputs();" class="spaced_label" checked=
                                            "checked"><label for="scale_2x">2x&nbsp;(Retina)</label></span> <span class="no_wrap"><input name="scale_x" type="radio" value="4" id="scale_4x" onchange="render_updated_outputs();" class=
                                            "spaced_label"><label for="scale_4x">4x&nbsp;(Impression)</label></span></p>

                                            <p class="center_para no_wrap download_link"><strong><a href="#" id="download_png_link" download="sankey.png">...</a></strong>
                                            </p>

                                            <p class="center_content" style="margin-bottom: 6px;">Dimensions pour la balise IMG :</p>

                                            <p class="center_content" id="img_tag_hint">
                                            </p>
                                        </div>
                                    </div>

                                    <form method="post" action={$self_url}>
                                        <h3 class="ui_head" onclick="toggle_panel('svg_options');"><span id="svg_options_indicator" class="indicator">⇓</span>Code <abbr title="Scalable Vector Graphics">SVG</abbr><span id=
                                        "svg_options_hint">...</span></h3>

                                        <div id="svg_options" class="form_chunk" style="display: none;">
                                            <div class="form_elements1">
                                                <textarea id="svg_for_export" name="svg" contenteditable="true" style="width:260px; height:130px; resize: both;">
                                                    (génération du code SVG....)
                                                </textarea>
                                                
                                                {if $enable_export}
                                                <p class="submit">
                                                    {csrf_field key="export_sankey"}
                                                    <input type="submit" name="save" value="Enregistrer &rarr;" />
                                                </p>
                                                {/if}

                                                <p class="form_elements">Copiez le code ci-dessus pour enregistrer votre diagramme.</p>

                                                <p class="form_elements">Enregistez le dans un fichier “<code>.svg</code>” pour le modifier dans un autre logiciel.</p>

                                                <p class="form_elements">Chaque lien aura une info-bulle en passant la souris au-dessus.</p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </table>
            </td>
            <td id="grapharea">
                <table id="messages">
                    <tr>
                        <td id="messages_area">
                        </td>
                    </tr>
                </table>

                <h2 class="ui_head" onclick="toggle_panel('layout_options');"><span id="layout_options_indicator" class="indicator">⇓</span> Options de disposition<span id="layout_options_hint">...</span></h2>

                <div id="layout_options" class="form_chunk" style="">
                    <div class="form_elements1" style="text-align: center;">
                        <table style="width: 100%; padding: 0px;">
                            <tbody>
                                <tr>
                                    <td style="text-align: left;"><input type="checkbox" id="justify_origins" value="1" checked=1 onchange="process_sankey();"> <label for="justify_origins">Coller tout les noeuds <strong>sources</strong> au bord gauche</label></td>
                                    <td style="text-align: right;"><input type="checkbox" id="justify_ends" value="1" checked=1 onchange="process_sankey();"> <label for="justify_ends">Coller tout les noeds <strong>de fin</strong> au bord droit</label></td>
                                </tr>
                            </tbody>
                        </table>
                        <small>(Note : ces options ne s'appliquent qu'aux diagrammes complexes avec 3, ou plus, colonnes de noeuds.)</small>
                    </div>
                </div>

                <p id="chart"><svg id="sankey_svg" height="600" width="600" xmlns="http://www.w3.org/2000/svg" version="1.1"></svg>
                </p>

                <canvas id="png_preview" height="600" width="600" style="background-color: transparent; display:none;">
                </canvas>

                <div id="chartfooter">
                </div>
            </td>
        </tr>
    </table>
</div>
<script>
process_sankey(); // render on page load
</script>

</div>