{include file="_head.tpl" title="Synthèse des projets — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="projets"}

<div class="year-header">
	<h2>{$config.nom_asso} — Projets</h2>
    <p>Cette vue synthétise les totaux de chaque projet, et affiche les sommes relatives aux comptes de résultats.</p>

	<p class="noprint print-btn">
		<button onclick="window.print(); return false;" class="icn-btn" data-icon="⎙">Imprimer</button>
	</p>
</div>

{if !empty($list)}
	<table class="list projects">
		<thead>
			<tr>
				<td>Année</td>
				<td></td>
				<td class="money">Charges</td>
				<td class="money">Produits</td>
				<td class="money">Résultat</td>
				<td class="money">Débits</td>
				<td class="money">Crédits</td>
				<td class="money">Solde</td>
			</tr>
		</thead>
		<tbody>

		{foreach from=$list item="parent"}
			<?php $result = $parent->sum_revenue - $parent->sum_expense; ?>
			<tr>
				<th>{$parent.label}</th>
				<td>
				<span class="noprint">
					| <a href="{$admin_url}acc/reports/trial_balance.php?project={$parent.id}">Balance générale</a>
					| <a href="{$admin_url}acc/reports/journal.php?project={$parent.id}">Journal général</a>
					| <a href="{$admin_url}acc/reports/ledger.php?project={$parent.id}">Grand livre</a>
					| <a href="{$admin_url}acc/reports/statement.php?project={$parent.id}">Compte de résultat</a>
				</span>
				</td>
				<td class="money">{$parent.sum_revenue|raw|money:false}</td>
				<td class="money">{$parent.sum_expense|raw|money:false}</td>
				<td class="money">{$result|raw|money}</td>
				<td class="money">{$parent.debit|raw|money:false}</td>
				<td class="money">{$parent.credit|raw|money:false}</td>
				<td class="money">{$parent.sum|raw|money:false}</td>
			</tr>
		{/foreach}
		</tbody>
	</table>
{else}
	<p class="block alert">
		Il n'y a pas de projet visible en cours.
		{if $current_year && !$analytical_accounts_count}
			{linkbutton label="Créer un nouveau compte de projet" href="!acc/charts/accounts/new.php?id=%d&type=%d"|args:$current_year.id_chart,$analytical_type shape="plus"}
		{else}
			Le solde des projets apparaîtra quand des écritures seront affectées à ces projets.
		{/if}
	</p>
{/if}

{include file="_foot.tpl"}