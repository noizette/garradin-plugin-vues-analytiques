{include file="_head.tpl" title="Configuration — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="config"}

{if $ok && !$form->hasErrors()}
    <p class="confirm">
        La configuration a bien été enregistrée.
    </p>
{/if}

{form_errors}

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Configuration du plugin</legend>
        <dl>
            <p>Attention, en activant cette option, vous permettez d'enregistrer un fichier avec n'importe quel contenu sur votre serveur, sans vérifications apportées à celui-ci. Soyez sûr·es de qui a accès à cette fonctionnalité.</p>
            {input type="checkbox" name="enable_export" label="Activer l'enregistrement du SVG sur le serveur" value=1 source=$cfg}
            {input type="text" name="export_path" label="Chemin pour stocker l'export SVG du diagramme" source=$cfg}
        </dl>
    </fieldset>

    <p class="submit">
        {csrf_field key="vues_config"}
        <input type="submit" name="save" value="Enregistrer &rarr;" class="main"/>
    </p>
</form>

{include file="_foot.tpl"}