{include file="_head.tpl" title="Recherches enregistrée — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{include file="%s/templates/_menu.tpl"|args:$plugin_root current="index"}


<nav class="tabs">
	<ul>
		<li><a href="search.php">Search</a></li>
		<li class="current"><a href="list.php">Recherches enregistrées</a></li>
	</ul>
</nav>


{if count($list) == 0}
	<p class="block alert">Aucune recherche enregistrée. <a href="{$search_url}">Faire une nouvelle recherche</a></p>
{else}
	<table class="list">
		<thead>
			<tr>
				<th>Search</th>
				<th>Type</th>
				<th>Statut</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$list item="recherche"}
			<tr>
				<th><a href="{$search_url}?id={$recherche.id}">{$recherche.label}</a></th>
				<td>{if $recherche.type == Entities\Search::TYPE_JSON}Avancée{else}SQL{/if}</td>
				<td>{if !$recherche.id_user}Publique{else}Privée{/if}</td>
				<td class="actions">
					{linkbutton href="%s?id=%d"|args:$search_url,$recherche.id shape="search" label="Afficher"}
					{linkbutton href="%s?id=%d"|args:$gar_search_url,$recherche.id shape="search" label="Search traditionnelle"}
				</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
{/if}


{include file="_foot.tpl"}
