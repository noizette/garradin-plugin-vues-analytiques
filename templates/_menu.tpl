<nav class="tabs">
<ul>
    <li{if $current == 'index'} class="current"{/if}><a href="{plugin_url file=""}">Search par mois</a></li>
    <li{if $current == 'projets'} class="current"{/if}><a href="{plugin_url file="projets.php"}">Vue projets synthétique</a></li>
    <li{if $current == 'sankey'} class="current"{/if}><a href="{plugin_url file="sankey.php"}">Sankey</a></li>
    <li{if $current == 'config'} class="current"{/if}><a href="{plugin_url file="config.php"}">Configuration</a></li>
</ul>
</nav>
