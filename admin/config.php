<?php

namespace Paheko;

$session->requireAccess($session::SECTION_CONFIG, $session::ACCESS_ADMIN);

$ok = false;

if (f('save') && $form->check('vues_config'))
{
    try {
        $plugin->setConfig('enable_export', f('enable_export'));
        $plugin->setConfig('export_path', f('export_path'));
        
		$ok = true;
    }
    catch (UserException $e)
    {
        $form->addError($e->getMessage());
    }
}

$cfg['enable_export'] = $plugin->getConfig('enable_export');
$cfg['export_path'] = $plugin->getConfig('export_path');
$tpl->assign(compact('ok', 'cfg'));

$tpl->display(PLUGIN_ROOT . '/templates/config.tpl');