<?php

namespace Paheko;

use Paheko\Accounting\Projects;

require_once __DIR__ . '/_inc.php';

$db = DB::getInstance();

$order = $db->getAssoc('SELECT id, label FROM acc_projects ORDER BY label;');
foreach (Projects::getBalances(false) as $projet)
{
	$label = $order[$projet->id];
	$order[$projet->id] = $projet;
	$order[$projet->id]->label = $label;
}

$tpl->assign('list', $order);
$tpl->display(PLUGIN_ROOT . '/templates/projets.tpl');