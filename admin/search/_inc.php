<?php

namespace Paheko;

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

require_once __DIR__ . '/../_inc.php';

$recherche = new Search;

const CURRENT_SEARCH_TARGET = 'accounting';
$target = 'accounting';
$search_url = PLUGIN_ADMIN_URL . 'search/search.php';
$gar_search_url = ADMIN_URL . 'acc/search.php';

$user = $session->getUser();

$tpl->assign(compact('target', 'search_url', 'gar_search_url', 'user'));
